# Locality-aware GPU Register File

This is the Locality-aware GPU register file code that was developed based on GPGPU-Sim for

**Hyeran Jeon, Hodjat Asghari Esfeden, Nael B. Abu-Ghazaleh, Daniel Wong, Sindhuja Elango, "Locality-Aware GPU Regsiter File," IEEE Computer Architecture Letter (CAL), Vol 18, Issue 2, July-Dec. 1 2019 ([https://ieeexplore.ieee.org/document/8931588](https://ieeexplore.ieee.org/document/8931588))**


* Compilation & Execution: The same with GPGPU-Sim

* Sample configuration: sample_gpgpusim.config

* How can I turn off locality-aware GPU register file ? : By commenting out -DREG_SHARING in the Makefiles.

* Expected simulation outputs: 
You can check the new statistics under the following keywords in the simulation output file:

> **-total load stop count** = 150						// total number of skipped load instructions
> 
> **-max phy2arch list length** = 10					// maximum physical reigster to arch register mapping list across SMs
> 
> **-min phy2arch list length** = 1					// minimum physical register to arch register mapping list across SMs
> 
> **-max no of shared phys per core**= 35				// maximum number of shared physical registers across SMs
> 
> **-min no of shared phys per core**= 30				// minimum number of shared physical registers across SMs
> 
> **-max no of addr to phy mapping per core**= 18		// maximum number of memory address to physical register mapping (address mapping table size)
> 
> **-min no of addr to phy mapping per core**= 16		// minimum number of memory address to physical register mapping 

